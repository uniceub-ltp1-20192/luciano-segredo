import sys

def askvalue(message):
    return int(input(message))

print('Considering an 8 semesters course')
age=askvalue('Insert your age: ')
semester=askvalue('Wich semester are you in? ')

if  semester>8:
    print('Your course must have 8 semesters! ')
    sys.exit()

def operation_sem(semester):
    return (8-semester)

def operation_age(semester,age):
    return age+(8-semester)-2

miss=operation_sem(semester)
age_finish=operation_age(semester,age)

if (semester==1 or semester==2):
    print('{} semester left to graduate'.format(miss),end='')
    print(', so {} years left to graduate'.format(miss - 3))  
    print('Will graduate with {} years '.format(age_finish-1))
    sys.exit()
if (semester==3 or semester==4):
    print('{} semester left to graduate'.format(miss), end='')
    print(', so {} years left to graduate'.format(miss-2))
    print('Will graduate with {} years '.format(age_finish))
    sys.exit()
if (semester==5 or semester==6):
    print('{} semester left to graduate'.format(miss), end='')
    print(', so {} years left to graduate'.format(miss - 1))  
    print('Will graduate with {} years '.format(age_finish+1))
    sys.exit()
if (semester==7 or semester==8):
    print('{} semester left to graduate'.format(miss), end='')
    print(', so {} years left to graduate'.format(miss))  # age_finish-age-1))
    print('Will graduate with {} years '.format(age_finish + 2))
    sys.exit()

