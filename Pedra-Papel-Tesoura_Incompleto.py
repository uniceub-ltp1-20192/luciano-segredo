import time
import random

def askstring(s):
    return (input(s))

def RandomChoice():
    Choice = random.randint(1, 3)
    if (Choice == 1):
        return 'Rock'
    if (Choice == 2):
        return 'Paper'
    return 'Scissors'

Name=askstring('What is your name? ')
user_choice=askstring('{}! Rock, Paper or Scissors? '.format(Name))

cpu_choice=RandomChoice()

def Conditions(user_choice, cpu_choice):
    if (user_choice == cpu_choice):
        return 'It is a Draw! '
    if (user_choice == 'Rock' and cpu_choice == 'Scissors' or user_choice == 'Paper' and cpu_choice == 'Rock' or user_choice == 'Scissors' and cpu_choice == 'Paper'):
        return 'You Win! '
    if (user_choice == 'Rock' and cpu_choice == 'Paper' or user_choice == 'Scissors' and cpu_choice == 'Rock' or user_choice == 'Paper' and cpu_choice == 'Scissors'):
        return 'You Lose!'

print('Let me Process')
print('...')
time.sleep(3)
Result = Conditions(user_choice, cpu_choice)
print('CPU: {}' .format(cpu_choice))
print(Result)
