import sys

def askvalue(message):
    return int(input(message))

print('Considering an 8 semesters course')
age=askvalue('Insert your age: ')
semester=askvalue('Wich semester are you in? ')
late=askvalue('How many semesters are you late? ')

if  semester>8:
    print('Your course must have 8 semesters! ')
    sys.exit()

def operation_sem(semester):
    return (8-semester)

def operation_age(semester,age):
    return age+(8-semester)-2

miss=operation_sem(semester)
age_finish=operation_age(semester,age)

def later(miss,late):
    return miss/2+late/2

def total(late):
    return (8+late)/2

tot=total(late)
graduate_late=later(miss,late)

if (semester==1 or semester==2):
    print('{} semester left to graduate'.format(miss),end='')
    print(', so {} years left to graduate'.format(miss - 3))
    print('Will graduate with {} years '.format(age_finish-1))
    print('You will graduate, with late, in {} years'.format(graduate_late))
    print('. So, you will have {} years of course!'.format(tot))
    sys.exit()
if (semester==3 or semester==4):
    print('{} semester left to graduate'.format(miss), end='')
    print(', so {} years left to graduate'.format(miss-2))
    print('Will graduate with {} years '.format(age_finish))
    print('You will graduate, with late, in {} years'.format(graduate_late))
    print('. So, you will have {} years of course!'.format(tot))
    sys.exit()
if (semester==5 or semester==6):
    print('{} semester left to graduate'.format(miss), end='')
    print(', so {} years left to graduate'.format(miss - 1))
    print('Will graduate with {} years '.format(age_finish+1))
    print('You will graduate, with late, in {} years'.format(graduate_late))
    print('. So, you will have {} years of course!'.format(tot))
    sys.exit()
if (semester==7 or semester==8):
    print('{} semester left to graduate'.format(miss), end='')
    print(', so {} years left to graduate'.format(miss))
    print('Will graduate with {} years '.format(age_finish + 2))
    print('You will graduate, with late, in {} years'.format(graduate_late))
    print('. So, you will have {} years of course!'.format(tot))
    sys.exit()

