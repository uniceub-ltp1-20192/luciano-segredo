numbers_cube=[]

def cube_list():
    for number in range(3,1000+1):
        cube=number**3
        numbers_cube.append(cube)
    return (numbers_cube)

print(cube_list())
print('\nThe cube list numbers are:')

for cube in range(3,1000+1):
    print(cube**3)
