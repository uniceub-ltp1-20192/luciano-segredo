cars = ['BMW', 'Volkswagen', 'Porsche', 'Mercedes', 'Fiat(just argo)']

def three_first():
    return cars[:3]

def last_three():
    return cars[-3:]

def medium():
    return cars[1:4]

print('The first three elements on list are: {}'.format(three_first()))
print('The last three elements on list are: {}'.format(last_three()))
print('The three elements in the middle of the list are: {}'.format(medium()))
