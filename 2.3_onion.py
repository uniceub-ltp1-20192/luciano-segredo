

#vari�vel que armazena o n�mero de cebolas
cebolas = 360

#vari�vel que armazena a quantidade de cebolas na caixa
cebolas_na_caixa = 240

#vari�vel que armazena a quantidade de cebolas que cabem em uma caixa
espaco_caixa = 12

#vari�vel que armazena o n�mero de caixas
caixas = 52

#vari�vel que armazena uma opera��o que tem como resultado quantidade de cebolas que n�o est�o encaixotadas
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa

#vari�vel que armazena uma opera��o que tem como resultado a quantidade de caixas vazias
caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa)

#vari�vel que armazena uma opera��o que tem como resultado a quantidade de caixas necess�rias para empacotar as cebolas sem caixa
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa

#imprimem as vari�veis acima, colocando-as em um contexto
print ("Existem", cebolas_na_caixa, "cebolas encaixotadas")
print ("Existem", cebolas_fora_da_caixa, "cebolas sem caixa")
print ("Em cada caixa cabem", espaco_caixa, "cebolas")
print ("Ainda temos,", caixas_vazias, "caixas vazias")
print ("Ent�o, precisamos de", caixas_necessarias, "caixas para empacotar todas as cebolas")