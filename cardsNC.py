import random

shackle=('Spades', 'Hearts', 'Diamonds', 'Clubs')
card_max_value=13
card_min_value=1

def generateCardDeck():

    cards=[]

    for shackles in shackle:
        for value in range(card_min_value,card_max_value+1):
            card=(shackles,value)
            cards.append(card)

    return cards

def generateShuffle():
    deck=generateCardDeck()
    random.shuffle(deck)
    return deck


def distributing():
    giv=random.choices(generateShuffle(), k=3)
    return giv

print(generateShuffle())
print('These are your cards: {}'.format(distributing()))

