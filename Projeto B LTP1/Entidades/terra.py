from Entidades.planeta import Planeta

class Terra(Planeta):
  def __init__(self,agua = "possui"):
    self._agua = agua

  @property
  def agua(self):
    return self._agua
  
  @agua.setter
  def agua(self,agua):
    self._agua = agua

  def habitantes(self):
    print("terrestres")


  def __str__(self):
    return'''
    Planeta:
    Identificador {}
    Raio: {}
    Cor: {}
    Massa: {}
    Material: {}
    habitantes: {}
    agua: {}
    '''.format(self.identificador,self.raio,self.cor,self.massa,self.material,self.habitantes,self.agua)