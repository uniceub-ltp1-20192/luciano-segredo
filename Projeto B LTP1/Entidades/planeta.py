class Planeta:
  def __init__(self, identificador = 0, raio=0.0, material="", cor="", massa=0.0):
    self._identificador = identificador
    self._raio = raio
    self._material = material
    self._cor = cor
    self._massa = massa

  @property
  def identificador(self):
    return self._identificador

  @identificador.setter
  def identificador(self,identificador):
    self._identificador = identificador


  @property
  def raio(self):
    return self._raio
  
  @raio.setter
  def raio(self,raio):
    self._raio = raio

  @property
  def material(self):
    return self._material
  
  @material.setter
  def material(self,material):
    self._material = material

  @property
  def cor(self):
    return self._cor
  
  @cor.setter
  def cor(self,cor):
    self._cor = cor

  @property
  def massa(self):
    return self._massa
  
  @massa.setter
  def massa(self,massa):
    self._massa = massa

  def habitantes(self):
    print("seres vivos")

  def rotacao(self):
    print("Tempo que gira em seu próprio eixo") 