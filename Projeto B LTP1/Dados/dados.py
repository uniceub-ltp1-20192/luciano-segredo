from Entidades.terra import Terra
from Validador.validador import Validador

class Dados:

    def __init__(self):
        self._dados = dict()
        self.identificador = 0

    def buscarPorIdentificador(self, identificador):
        if (len(self._dados) == 0):
            print ("Dicionario vazio!")
        else:
            return self._dados.get(int(identificador))

    def buscarpPorAtributo(self, param):
        lista = dict()
        ultimoAchado = 0
        if (len(self._dados) == 0):
            print ("Dicionario vazio!")
        else:
            '''o metodo values do dicionario retorna cada valor incluso no dicionario '''
            x = Terra()
            for x in self._dados.values():
                if x.raio == param:
                    lista[x.identificador]=x
                    ultimoAchado = x.identificador
                if (len(lista)==1):
                    return lista[ultimoAchado]
                else:
                    if (len(lista)==0):
                        print("Dicionario vazio. ")
                        return None
                    return self.buscarPorIdentificadorComLista(lista)

    def buscarPorIdentificadorComLista(self,lista):
        print("Existem varios registros com o filtro que foi inserido. ")
        for x in lista.values():
            print(x)
        print("Informe o identificador do objeto: ")
        return lista.get(int(Validador.verificarInteiro()))

    def inserir (self, entidade):
        entidade.identificador = self.gerarProxIdentificador()
        '''salva a entidade dentro de uma posicao do dicionario utilizando o identificador'''
        self._dados[entidade.identificador] = entidade

    def alterar (self, entidade):
        self._dados[entidade.identificador] = entidade

    def deletar(self, entidade):
        del self._dados[entidade.identificador]

    def gerarProxIdentificador(self):
        self.identificador=self.identificador + 1
        return self.identificador


    def criarTerra(self):
        t = Terra()
        t.cor = "azul"
        t.raio = 6371
        t.massa = 5972
        t.material = "Rochoso"
