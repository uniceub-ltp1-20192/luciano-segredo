import re

class Validador:
    @staticmethod
    def verificarInteiro():
        teste = False
        while teste == False:
            val = input("Informe um inteiro: ")
            v = re.match("\d+",val)
            if v != None:
                teste = True
        return int(val)        


    @staticmethod
    def validarOpMenu(expReg):
        teste = False
        while teste == False:
            opcao = input("Informe uma opcao: ")
            val = re.match(expReg,opcao)
            if val != None:
                return opcao
            else:
                print("Opcao Invalida! Insira um valor entre {}".format(expReg))


    @staticmethod
    def validarValorInformado(valorNow,textoMSG):
        novoValor = input(textoMSG)
        if novoValor != None and novoValor != "":
            return novoValor
        return valorNow

    





