
#variável que armazena o número de cebolas
cebolas = 300

#variável que armazena a quantidade de cebolas na caixa
cebolas_caixa = 120

#variável que armazena a quantidade de cebolas que cabem em uma caixa
espaco_caixa = 5

#variável que armazena o número de caixas
caixas = 60

#função que retorna uma operação que tem como resultado quantidade de cebolas que não estão encaixotadas
def cebolas_fora_caixa(cebolas,cebolas_caixa):
    return cebolas - cebolas_caixa

#função que retorna uma operação que tem como resultado a quantidade de caixas vazias
def caixas_vazias(cebolas_caixa,espaco_caixa,caixas):
    return caixas - (cebolas_caixa/espaco_caixa)

#função que retorna uma operação que tem como resultado a quantidade de caixas necessárias para empacotar as cebolas sem caixa
def caixas_necessarias(cebolas_fora_caixa,espaco_caixa):
    return cebolas_fora_caixa / espaco_caixa

#armazenando as funções em variáveis
result_cebola_sem=cebolas_fora_caixa(cebolas,cebolas_caixa)
resulta_caixa_sem=caixas_vazias(cebolas_caixa,espaco_caixa,caixas)
result_caixas_precisa=caixas_necessarias(cebolas_fora_caixa(cebolas,cebolas_caixa),espaco_caixa)

#imprimem as variáveis acima, colocando-as em um contexto
print ("Existem", cebolas_caixa, "cebolas encaixotadas")
print ("Existem", result_cebola_sem, "cebolas sem caixa")
print ("Em cada caixa cabem", espaco_caixa, "cebolas")
print ("Ainda temos,", resulta_caixa_sem, "caixas vazias")
print ("Então, precisamos de", result_caixas_precisa, "caixas para empacotar todas as cebolas")
