things=[]

def askOP(b):
    return(input(b))

def question():

    while True:

        option=input('Do you want insert more numbers in list? ')

        if option=='Y':
            return True
        if option=='N':
            return False


def timesList(things):
    multiply=1
    for thing in things:
        multiply*=thing
    return multiply

loop=1
while loop:
    things.append(int(input('Insert a number in list: ')))
    time_list=timesList(things)
    print('Answer with (Y) for yes or (N) for no! ')
    loop=question()

print('That is your list: ',end='')
print(things)
print('The multiplicate of all elements in list is: {}'.format(timesList(things)))
