
from Entidades.cachorro import Dog

class Beagle(Dog):
  def __init__(self,TipoOrelha = "grande"):
    self._TipoOrelha = TipoOrelha

  @property
  def TipoOrelha(self):
    return self._TipoOrelha
  
  @TipoOrelha.setter
  def TipoOrelha(self,TipoOrelha):
    self._TipoOrelha = TipoOrelha

  def latir(self):
    print("rhuuuuuuu")