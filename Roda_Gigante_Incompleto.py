import sys

#Fun��o que valida os valores
def validatestring(s):
    return (not(str.__contains__(s,'1234567890!@#$%&*(,.;?')))

#Fun��o que pede para inserir algo
def askstring(s):
    return (input(s))

#Solicita o nome
name=askstring('Name: ')
if(not(validatestring(name))):
    print('Error! ')
    sys.exit()

#Solicita a altura
height=askstring('Height: ')
if(not(validatestring(height))):
    print('Error! ')
    sys.exit()

#Solicita o n�mero de acompanhantes
friends=askstring('Friends: ')
if(not(validatestring(friends))):
    print('Error! ')
    sys.exit()

print('Hello {}, you have {}m of height and you are with {} friends!'.format(name, height, friends))

